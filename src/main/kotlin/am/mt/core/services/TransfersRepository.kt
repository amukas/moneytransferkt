package am.mt.core.services

import am.mt.core.domain.MoneyTransfer

interface TransfersRepository {

    fun findById(moneyTransferId: Long): MoneyTransfer?

    fun saveOrUpdate(moneyTransfer: MoneyTransfer)
}
