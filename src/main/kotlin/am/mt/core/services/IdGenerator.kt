package am.mt.core.services

interface IdGenerator {

    fun nextId(): Long
}
