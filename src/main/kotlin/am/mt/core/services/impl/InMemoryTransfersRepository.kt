package am.mt.core.services.impl

import am.mt.core.domain.MoneyTransfer
import am.mt.core.services.TransfersRepository
import java.util.concurrent.ConcurrentHashMap

class InMemoryTransfersRepository : TransfersRepository {

    private val transfers = ConcurrentHashMap<Long, MoneyTransfer>()

    override fun findById(moneyTransferId: Long): MoneyTransfer? {
        return transfers[moneyTransferId]
    }

    override fun saveOrUpdate(moneyTransfer: MoneyTransfer) {
        transfers[moneyTransfer.id] = moneyTransfer
    }

    private fun checkIfExist(transferId: Long) {
        if (transfers.containsKey(transferId).not()) {
            throw RuntimeException("Transfer [$transferId] does not exist.")
        }
    }
}
