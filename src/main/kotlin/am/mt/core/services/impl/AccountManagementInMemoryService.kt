package am.mt.core.services.impl

import am.mt.core.domain.Account
import am.mt.core.domain.Currency
import am.mt.core.services.AccountManagementService
import am.mt.core.services.IdGenerator
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock


class AccountManagementInMemoryService(private val idGen: IdGenerator) : AccountManagementService {
    private val storage = ConcurrentHashMap<Long, Account>()
    private val locks = ConcurrentHashMap<Long, Lock>()

    override fun lockAccount(accountId: Long): Account {
        checkIfExist(accountId)
        locks.computeIfAbsent(accountId) { ReentrantLock() }.lock()
        return storage[accountId]!!
    }

    override fun unlockAccount(accountId: Long) {
        checkIfExist(accountId)
        locks.computeIfAbsent(accountId) { ReentrantLock() }.unlock()
    }

    override fun accountExists(accountId: Long): Boolean {
        return storage.containsKey(accountId)
    }

    override fun findById(accountId: Long): Account? {
        return this.storage[accountId]
    }

    override fun createAccount(accountHolder: String): Account {
        val account = Account(idGen.nextId(), accountHolder)
        this.storage[account.id] = account
        return account
    }

    override fun updateAccount(vararg accounts: Account) {
        for (account in accounts) {
            this.storage[account.id] = account
        }
    }

    override fun topUp(accountId: Long, currency: Currency, amount: Long): Account {
        try {
            val account = lockAccount(accountId)
            val updatedBalance = account.balances.toMutableMap()
            updatedBalance[currency] = account.balanceFor(currency) + amount
            val updatedAccount = account.copy(balances = updatedBalance.toMap())

            this.storage[updatedAccount.id] = updatedAccount
            return updatedAccount
        } finally {
            unlockAccount(accountId)
        }
    }

    private fun checkIfExist(accountId: Long) {
        if (storage.containsKey(accountId).not()) {
            throw RuntimeException("Account [$accountId] does not exist.")
        }
    }
}
