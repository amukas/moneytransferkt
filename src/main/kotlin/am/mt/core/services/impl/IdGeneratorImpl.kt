package am.mt.core.services.impl

import am.mt.core.services.IdGenerator
import java.util.concurrent.atomic.AtomicLong

class IdGeneratorImpl : IdGenerator {

    private val currentId = AtomicLong(0)

    override fun nextId(): Long = currentId.incrementAndGet()
}
