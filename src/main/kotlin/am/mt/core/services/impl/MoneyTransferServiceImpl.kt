package am.mt.core.services.impl

import am.mt.core.domain.Account
import am.mt.core.domain.Currency
import am.mt.core.domain.MoneyTransfer
import am.mt.core.domain.MoneyTransferStatus
import am.mt.core.services.AccountManagementService
import am.mt.core.services.IdGenerator
import am.mt.core.services.MoneyTransferService
import am.mt.core.services.TransfersRepository

class MoneyTransferServiceImpl(
        private val idGenerator: IdGenerator,
        private val accountManagement: AccountManagementService,
        private val transfersRepository: TransfersRepository
) : MoneyTransferService {

    override fun findById(moneyTransferId: Long): MoneyTransfer? {
        return transfersRepository.findById(moneyTransferId)
    }

    override fun transfer(fromAccountId: Long, toAccountId: Long, currency: Currency, amount: Long): MoneyTransfer {
        val transfer = withLocking(fromAccountId, toAccountId) { from, to, transfer ->
            if (!from.hasEnoughBalance(currency, amount)) {
                return@withLocking transfer.failed("Not enough balance")
            }
            val updatedFrom = from.withdraw(currency, amount)
            val updatedTo = to.deposit(currency, amount)
            accountManagement.updateAccount(updatedFrom, updatedTo) // to updated atomically
            transfer.succeed()
        }

        transfersRepository.saveOrUpdate(transfer)
        return transfer
    }

    private fun withLocking(fromAccountId: Long, toAccountId: Long, logic: (Account, Account, MoneyTransfer) -> MoneyTransfer): MoneyTransfer {
        //to avoid dead locks, maintain same locking order
        val useReverseLock = fromAccountId > toAccountId

        try {
            val (fromAccount: Account, toAccount: Account) = if (useReverseLock) {
                Pair(accountManagement.lockAccount(toAccountId), accountManagement.lockAccount(fromAccountId))
            } else {
                Pair(accountManagement.lockAccount(fromAccountId), accountManagement.lockAccount(toAccountId))
            }

            val moneyTransfer = MoneyTransfer(
                    id = idGenerator.nextId(),
                    status = MoneyTransferStatus.CREATED,
                    fromAccountId = fromAccountId,
                    toAccountId = toAccountId)

            return logic(fromAccount, toAccount, moneyTransfer)
        } finally {
            if (useReverseLock) {
                accountManagement.unlockAccount(fromAccountId)
                accountManagement.unlockAccount(toAccountId)
            } else {
                accountManagement.unlockAccount(toAccountId)
                accountManagement.unlockAccount(fromAccountId)
            }
        }
    }
}
