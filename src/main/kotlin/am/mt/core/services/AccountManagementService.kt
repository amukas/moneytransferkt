package am.mt.core.services

import am.mt.core.domain.Account
import am.mt.core.domain.Currency

interface AccountManagementService {

    fun lockAccount(accountId: Long): Account

    fun unlockAccount(accountId: Long)

    fun accountExists(accountId: Long): Boolean

    fun findById(accountId: Long): Account?

    fun createAccount(accountHolder: String): Account

    /**
     * Atomically updates multiple accounts.
     */
    fun updateAccount(vararg accounts: Account)

    fun topUp(accountId: Long, currency: Currency, amount: Long): Account
}
