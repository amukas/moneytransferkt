package am.mt.core.services

import am.mt.core.domain.Currency
import am.mt.core.domain.MoneyTransfer

interface MoneyTransferService {

    fun findById(moneyTransferId: Long): MoneyTransfer?

    fun transfer(fromAccountId: Long, toAccountId: Long, currency: Currency, amount: Long): MoneyTransfer
}
