package am.mt.core.domain


data class Account(
        val id: Long,
        val holder: String,
        val balances: Map<Currency, Long> = emptyMap()
) {

    fun balanceFor(currency: Currency): Long {
        return balances[currency] ?: return 0L
    }

    fun hasEnoughBalance(currency: Currency, amount: Long): Boolean {
        return balanceFor(currency) >= amount
    }

    fun withdraw(currency: Currency, amount: Long): Account {
        if (!hasEnoughBalance(currency, amount)) {
            throw RuntimeException("Not enough balance")
        }

        val updatedBalance = this.balances.toMutableMap()
        updatedBalance.computeIfPresent(currency) { _, balance -> balance - amount }

        return copy(balances = updatedBalance.toMap())
    }

    fun deposit(currency: Currency, amount: Long): Account {
        val updatedBalance = this.balances.toMutableMap()
        updatedBalance.compute(currency) { _, balance -> (balance ?: 0) + amount }

        return copy(balances = updatedBalance.toMap())
    }
}
