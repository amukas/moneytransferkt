package am.mt.core.domain

enum class Currency(val code: String) {

    USD("USD"),
    EUR("EUR");

    companion object {
        fun fromCode(code: String): Currency {
            for (each in values()) {
                if (each.code.equals(code, ignoreCase = true)) {
                    return each
                }
            }
            throw RuntimeException("[$code] is not valid")
        }
    }
}
