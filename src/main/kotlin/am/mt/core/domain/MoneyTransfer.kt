package am.mt.core.domain

import am.mt.core.domain.MoneyTransferStatus.FAILED
import am.mt.core.domain.MoneyTransferStatus.SUCCEED

data class MoneyTransfer(
        val id: Long,
        val fromAccountId: Long,
        val toAccountId: Long,
        val status: MoneyTransferStatus,
        val statusDescription: String = ""
) {

    fun failed(description: String): MoneyTransfer = copy(status = FAILED, statusDescription = description)

    fun succeed(): MoneyTransfer = copy(status = SUCCEED)
}
