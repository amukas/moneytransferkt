package am.mt.core.domain

enum class MoneyTransferStatus {
    CREATED,
    IN_PROGRESS,
    SUCCEED,
    FAILED
}
