package am.mt.core.utils

fun Double.toRawAmount(): Long = (this * 100).toLong()
fun Long.fromRawAmount(): Double = this / 100.0
