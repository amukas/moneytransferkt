package am.mt.api.rest

import am.mt.api.rest.common.ErrorResp
import am.mt.api.rest.common.WithModelValidation
import am.mt.core.domain.Account
import am.mt.core.domain.Currency
import am.mt.core.services.AccountManagementService
import am.mt.core.utils.fromRawAmount
import am.mt.core.utils.toRawAmount
import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.get
import spark.Spark.post
import java.util.*
import javax.servlet.http.HttpServletResponse.*

class AccountsRestApi(
        private val mapper: Gson,
        private val accountManagement: AccountManagementService
) {

    fun initialize() {
        post("/accounts", ::createAccount)
        get("/accounts/:id", ::findAccount)
        post("/accounts/:id/top-ups", ::topUpAccount)
    }

    private fun createAccount(req: Request, resp: Response): Any {
        val createReq = mapper.fromJson(req.body(), CreateReq::class.java)
        val error = createReq.validate()
        if (error != null) {
            resp.status(SC_BAD_REQUEST)
            return error
        }

        val account = accountManagement.createAccount(createReq.accountHolder!!)
        resp.status(SC_CREATED)
        return AccountResp.fromAccount(account)
    }

    private fun findAccount(req: Request, resp: Response): Any? {
        val findAccountReq = FindAccountReq(req.params("id"))
        val error = findAccountReq.validate()
        if (error != null) {
            resp.status(SC_BAD_REQUEST)
            return error
        }

        val account = accountManagement.findById(findAccountReq.idAsLong())
        return if (account == null) {
            resp.status(SC_NOT_FOUND)
            null
        } else {
            resp.status(SC_OK)
            AccountResp.fromAccount(account)
        }
    }

    private fun topUpAccount(req: Request, resp: Response): Any {
        val topUpReq = mapper.fromJson(req.body(), TopUpReq::class.java)
        topUpReq.id = req.params("id")
        val error = topUpReq.validate(accountManagement)
        if (error != null) {
            resp.status(SC_BAD_REQUEST)
            return error
        }

        val account = accountManagement.topUp(topUpReq.idAsLong(), topUpReq.getCurrency(), topUpReq.amountAsDouble().toRawAmount())
        resp.status(SC_CREATED)
        return AccountResp.fromAccount(account)
    }

    // ------

    private class CreateReq : WithModelValidation {
        val accountHolder: String? = null

        fun validate(): ErrorResp? {
            return validating(this)
                    .nonNull("accountHolder", { m -> m.accountHolder })
                    .done()
        }
    }

    private data class FindAccountReq(
            val id: String? = null
    ) : WithModelValidation {

        fun idAsLong() = this.id!!.toLong()

        fun validate(): ErrorResp? {
            return validating(this)
                    .nonNull("id", { m -> m.id },
                            { v -> v.isValid("id", { m -> m.id!!.toLong() > 0 }) })
                    .done()
        }
    }

    private data class TopUpReq(
            var id: String? = null,
            val currency: String? = null,
            val amount: String? = null
    ) : WithModelValidation {

        fun idAsLong() = this.id!!.toLong()

        fun amountAsDouble() = this.amount!!.toDouble()

        fun getCurrency() = Currency.fromCode(currency!!)

        @Suppress("NAME_SHADOWING")
        fun validate(accountManagement: AccountManagementService): ErrorResp? {
            return validating(this)
                    .nonNull("id", { m -> m.id }, { v ->
                        v.isValid("id", { m -> m.id!!.toLong() > 0 },
                                { v -> v.isValid("id", { m -> accountManagement.accountExists(m.idAsLong()) }) })
                    })
                    .nonNull("currency", { m -> m.currency }, { v ->
                        v.isValid("currency", { m -> Currency.fromCode(m.currency!!) })
                    })
                    .nonNull("amount", { m -> m.amount }, { v ->
                        v.isValid("amount", { m -> m.amount!!.toDouble() > 0 })
                    })
                    .done()
        }
    }

    private data class AccountResp(
            val id: Long = 0,
            val holder: String? = null,
            val balances: Map<Currency, Double> = emptyMap()
    ) {

        companion object {
            fun fromAccount(account: Account): AccountResp {
                val rawBalance = account.balances.toMutableMap()
                val balances = HashMap<Currency, Double>(rawBalance.size)
                rawBalance.forEach { (c, b) -> balances[c] = b.fromRawAmount() }

                return AccountResp(
                        id = account.id,
                        holder = account.holder,
                        balances = balances)
            }
        }
    }
}
