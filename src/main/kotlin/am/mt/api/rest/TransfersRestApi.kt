package am.mt.api.rest

import am.mt.api.rest.common.ErrorResp
import am.mt.api.rest.common.WithModelValidation
import am.mt.core.domain.Currency
import am.mt.core.domain.MoneyTransfer
import am.mt.core.domain.MoneyTransferStatus
import am.mt.core.services.AccountManagementService
import am.mt.core.services.MoneyTransferService
import am.mt.core.utils.toRawAmount
import com.google.gson.Gson
import spark.Request
import spark.Response
import spark.Spark.get
import spark.Spark.post
import javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST
import javax.servlet.http.HttpServletResponse.SC_CREATED

@Suppress("UNUSED_PARAMETER")
class TransfersRestApi(
        private val mapper: Gson,
        private val accountManagement: AccountManagementService,
        private val moneyTransfer: MoneyTransferService
) {

    fun initialize() {
        post("/transfers", ::createTransfer)
        get("/transfers/:id", ::findTransfer)
    }

    private fun createTransfer(req: Request, resp: Response): Any {
        val createReq = mapper.fromJson(req.body(), CreateReq::class.java)
        val error = createReq.validate(accountManagement)
        if (error != null) {
            resp.status(SC_BAD_REQUEST)
            return error
        }

        resp.status(SC_CREATED)
        val transfer = moneyTransfer.transfer(
                createReq.fromAccountIdAsLong(), createReq.toAccountIdAsLong(),
                createReq.getCurrency(), createReq.amountAsDouble().toRawAmount())
        return TransferResp.fromAccount(transfer)
    }

    private fun findTransfer(req: Request, resp: Response): Any? {
        val id = req.params("id").toLong()
        val transfer = moneyTransfer.findById(id)
        return if (transfer != null) TransferResp.fromAccount(transfer) else null
    }

    // ------

    private data class CreateReq(
            val fromAccountId: String? = null,
            val toAccountId: String? = null,
            val currency: String? = null,
            val amount: String? = null
    ) : WithModelValidation {

        fun fromAccountIdAsLong() = fromAccountId!!.toLong()

        fun toAccountIdAsLong() = toAccountId!!.toLong()

        fun amountAsDouble() = amount!!.toDouble()

        fun getCurrency(): Currency = Currency.fromCode(currency!!)

        @Suppress("NAME_SHADOWING")
        fun validate(accountManagement: AccountManagementService): ErrorResp? {
            return validating(this)
                    .nonNull("fromAccountId", { m -> m.fromAccountId }, { v ->
                        v.isValid("fromAccountId", { m -> m.fromAccountId!!.toLong() > 0 },
                                { v -> v.isValid("fromAccountId", { m -> accountManagement.accountExists(m.fromAccountIdAsLong()) }) })
                    })
                    .nonNull("toAccountId", { m -> m.toAccountId }, { v ->
                        v.isValid("toAccountId", { m -> m.toAccountId!!.toLong() > 0 },
                                { v -> v.isValid("toAccountId", { m -> accountManagement.accountExists(m.toAccountIdAsLong()) }) })
                    })
                    .nonNull("currency", { m -> m.currency }, { v ->
                        v.isValid("currency", { m -> Currency.fromCode(m.currency!!) })
                    })
                    .nonNull("amount", { m -> m.amount }, { v ->
                        v.isValid("amount", { m -> m.amount!!.toDouble() > 0 })
                    })
                    .done()
        }
    }

    private data class TransferResp(
            val id: Long,
            val fromAccountId: Long,
            val toAccountId: Long,
            val status: MoneyTransferStatus,
            val statusDescription: String?
    ) {
        companion object {
            fun fromAccount(transfer: MoneyTransfer): TransferResp {
                return TransferResp(
                        id = transfer.id,
                        fromAccountId = transfer.fromAccountId,
                        toAccountId = transfer.toAccountId,
                        status = transfer.status,
                        statusDescription = transfer.statusDescription
                )
            }
        }
    }
}
