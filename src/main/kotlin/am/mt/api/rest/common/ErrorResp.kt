package am.mt.api.rest.common

data class ErrorResp(val description: String, val errors: List<Error>) {

    data class Error(val message: String)
}
