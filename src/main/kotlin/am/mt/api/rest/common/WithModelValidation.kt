package am.mt.api.rest.common

import java.util.*

interface WithModelValidation {

    fun <T> validating(model: T): Validating<T> {
        return Validating(model)
    }

    class Validating<T>(private val model: T) {
        private val errors = ArrayList<ErrorResp.Error>()

        fun <X> nonNull(name: String, extractor: (T) -> X, vararg other: (Validating<T>) -> Validating<T>): Validating<T> {
            val candidate = extractor(model)
            if (candidate == null) {
                this.errors.add(ErrorResp.Error("[$name] is not set"))
            }
            other.forEach { it(this) }
            return this
        }

        fun isValid(name: String, extractor: (T) -> Any?, vararg other: (Validating<T>) -> Validating<T>): Validating<T> {
            val valid: Boolean = try {
                when (val r = extractor(model)) {
                    is Boolean -> r
                    else -> r != null
                }
            } catch (e: Exception) {
                false
            }

            if (!valid) {
                this.errors.add(ErrorResp.Error("[$name] is not valid"))
            }

            other.forEach { it(this) }

            return this
        }

        fun done(): ErrorResp? {
            return if (errors.isEmpty()) null else ErrorResp("Request is not valid", this.errors)
        }
    }
}
