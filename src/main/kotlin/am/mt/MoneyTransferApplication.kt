package am.mt

import am.mt.api.rest.AccountsRestApi
import am.mt.api.rest.TransfersRestApi
import am.mt.core.services.impl.AccountManagementInMemoryService
import am.mt.core.services.impl.IdGeneratorImpl
import am.mt.core.services.impl.InMemoryTransfersRepository
import am.mt.core.services.impl.MoneyTransferServiceImpl
import com.google.gson.GsonBuilder
import spark.Spark
import java.util.concurrent.atomic.AtomicBoolean

fun main(args: Array<String>) {
    val port = if (args.size == 1) Integer.parseInt(args[0]) else 8888
    MoneyTransferApplication().run(port)
}

class MoneyTransferApplication {

    private val stated = AtomicBoolean()

    fun run(port: Int) {
        stated.compareAndSet(false, true)

        val mapper = GsonBuilder()
                .setLenient()
                .create()
        val idGen = IdGeneratorImpl()
        val accountManagement = AccountManagementInMemoryService(idGen)
        val transfersRepository = InMemoryTransfersRepository()
        val moneyTransfer = MoneyTransferServiceImpl(idGen, accountManagement, transfersRepository)

        Spark.port(port)
        Spark.defaultResponseTransformer { model -> if (model != null) mapper.toJson(model) else null }

        TransfersRestApi(mapper, accountManagement, moneyTransfer).initialize()
        AccountsRestApi(mapper, accountManagement).initialize()
    }
}
