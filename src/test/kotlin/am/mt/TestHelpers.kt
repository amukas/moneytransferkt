package am.mt

import java.net.ServerSocket

object TestHelpers {
    fun availablePort(): Int {
        ServerSocket(0).use { socket -> return socket.localPort }
    }
}
