package am.mt

import am.mt.TestHelpers.availablePort
import am.mt.core.domain.Account
import am.mt.core.domain.Currency
import am.mt.core.domain.MoneyTransfer
import am.mt.core.domain.MoneyTransferStatus
import com.google.gson.GsonBuilder
import org.spekframework.spek2.Spek
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import spark.Spark
import strikt.api.expectThat
import strikt.assertions.contains
import strikt.assertions.isEqualTo
import javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST

object MoneyTransferAcceptanceTest : Spek({

    val port = availablePort()
    MoneyTransferApplication().run(port)

    val mapper = GsonBuilder()
            .setLenient()
            .create();

    val retrofit = Retrofit.Builder()
            .baseUrl("http://localhost:${port}/")
            .addConverterFactory(GsonConverterFactory.create(mapper))
            .build();
    val accountsApi by memoized { retrofit.create(AccountsApi::class.java) }
    val transfersApi by memoized { retrofit.create(TransfersApi::class.java) }

    afterGroup {
        Spark.stop()
    }

    test("happy path") {
        val currency = Currency.USD

        // accounts setup
        var john = accountsApi.createAccount(AccountCreateReq("John Doe"))
        john = accountsApi.topUpAccount(john.id, TopUpReq(currency, 100.0))
        val jane = accountsApi.createAccount(AccountCreateReq("Jane Doe"))

        val transfer: MoneyTransfer = transfersApi.createTransfer(TransferCreateReq(
                fromAccountId = john.id,
                toAccountId = jane.id,
                currency = currency,
                amount = 50.0
        ))

        expectThat(transfer.status).isEqualTo(MoneyTransferStatus.SUCCEED)

        expectThat(accountsApi.findAccount(john.id).balanceFor(currency)).isEqualTo(50)
        expectThat(accountsApi.findAccount(jane.id).balanceFor(currency)).isEqualTo(50)
    }

    fun performTransfer(reqModifier: (TransferCreateReqRaw) -> TransferCreateReqRaw): Response<MoneyTransfer> {
        val currency = Currency.USD

        // accounts setup
        var john = accountsApi.createAccount(AccountCreateReq("John Doe"))
        john = accountsApi.topUpAccount(john.id, TopUpReq(currency, 100.0))
        val jane = accountsApi.createAccount(AccountCreateReq("Jane Doe"))

        return transfersApi.createTransferRaw(reqModifier(TransferCreateReqRaw(
                fromAccountId = john.id.toString(),
                toAccountId = jane.id.toString(),
                currency = currency.toString(),
                amount = "50.0"
        ))).execute()
    }

    group("create account request validation") {
        test("fromAccountId doesn't exist") {
            with(performTransfer {
                it.copy(fromAccountId = "100")
            }) {
                expectThat(code()).isEqualTo(SC_BAD_REQUEST)
                expectThat(String(errorBody()!!.bytes())).contains("[fromAccountId] is not valid")
            }
        }
        test("fromAccountId not an integer") {
            with(performTransfer {
                it.copy(fromAccountId = "abc")
            }) {
                expectThat(code()).isEqualTo(SC_BAD_REQUEST)
                expectThat(String(errorBody()!!.bytes())).contains("[fromAccountId] is not valid")
            }
        }
        test("toAccountId doesn't exist") {
            with(performTransfer {
                it.copy(toAccountId = "100")
            }) {
                expectThat(code()).isEqualTo(SC_BAD_REQUEST)
                expectThat(String(errorBody()!!.bytes())).contains("[toAccountId] is not valid")
            }
        }
        test("toAccountId not an integer") {
            with(performTransfer {
                it.copy(toAccountId = "abc")
            }) {
                expectThat(code()).isEqualTo(SC_BAD_REQUEST)
                expectThat(String(errorBody()!!.bytes())).contains("[toAccountId] is not valid")
            }
        }
        test("not supported currency") {
            with(performTransfer {
                it.copy(currency = "PLN")
            }) {
                expectThat(code()).isEqualTo(SC_BAD_REQUEST)
                expectThat(String(errorBody()!!.bytes())).contains("[currency] is not valid")
            }
        }
        test("invalid amount") {
            with(performTransfer {
                it.copy(amount = "-1")
            }) {
                expectThat(code()).isEqualTo(SC_BAD_REQUEST)
                expectThat(String(errorBody()!!.bytes())).contains("[amount] is not valid")
            }
        }
        test("invalid is not an integer") {
            with(performTransfer {
                it.copy(amount = "abc")
            }) {
                expectThat(code()).isEqualTo(SC_BAD_REQUEST)
                expectThat(String(errorBody()!!.bytes())).contains("[amount] is not valid")
            }
        }
    }

})

fun <T> Call<T>.unwrap(): T {
    return this.execute().body()!!
}

fun AccountsApi.createAccount(req: AccountCreateReq) = createAccountRaw(req).unwrap()
fun AccountsApi.findAccount(accountId: Long) = findAccountRaw(accountId).unwrap()
fun AccountsApi.topUpAccount(accountId: Long, req: TopUpReq) = topUpAccountRaw(accountId, req).unwrap()

interface AccountsApi {
    @POST("/accounts")
    fun createAccountRaw(@Body req: AccountCreateReq): Call<Account>

    @GET("/accounts/{id}")
    fun findAccountRaw(@Path("id") accountId: Long): Call<Account>

    @POST("/accounts/{id}/top-ups")
    fun topUpAccountRaw(@Path("id") accountId: Long, @Body req: TopUpReq): Call<Account>
}


fun TransfersApi.createTransfer(req: TransferCreateReq) = createTransferRaw(req).unwrap()
fun TransfersApi.createTransfer(req: TransferCreateReqRaw) = createTransferRaw(req).unwrap()
fun TransfersApi.findTransferRaw(transferId: Long) = findTransferRaw(transferId).unwrap()

interface TransfersApi {
    @POST("/transfers")
    fun createTransferRaw(@Body req: TransferCreateReq): Call<MoneyTransfer>

    @POST("/transfers")
    fun createTransferRaw(@Body req: TransferCreateReqRaw): Call<MoneyTransfer>

    @GET("/transfers/{id}")
    fun findTransferRaw(@Path("id") transferId: Long): Call<MoneyTransfer>
}

data class AccountCreateReq(
        val accountHolder: String?
)

data class TransferCreateReq(
        val fromAccountId: Long?,
        val toAccountId: Long?,
        val currency: Currency,
        val amount: Double
)

data class TransferCreateReqRaw(
        val fromAccountId: String?,
        val toAccountId: String?,
        val currency: String?,
        val amount: String?
)

data class TopUpReq(
        val currency: Currency?,
        val amount: Double?
)
