package am.mt.core.services.impl

import am.mt.core.domain.Account
import am.mt.core.domain.Currency
import am.mt.core.domain.MoneyTransferStatus
import am.mt.core.utils.fromRawAmount
import am.mt.core.utils.toRawAmount
import org.spekframework.spek2.Spek
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.abs

object MoneyTransferServiceImplTest : Spek({

    val idGenerator = IdGeneratorImpl()
    val accountManagement = AccountManagementInMemoryService(idGenerator)
    val transfersRepository = InMemoryTransfersRepository()
    val sut = MoneyTransferServiceImpl(idGenerator, accountManagement, transfersRepository)

    test("simple money transfer") {
        val from = Account(idGenerator.nextId(), "HolderA", mapOf(Currency.USD to 100.00.toRawAmount()))
        val to = Account(idGenerator.nextId(), "HolderB")
        accountManagement.updateAccount(from, to)
        val currencyToTransfer = Currency.USD
        val amountToTransfer = 30.0.toRawAmount()

        val transfer = sut.transfer(from.id, to.id, currencyToTransfer, amountToTransfer)

        with(transfer) {
            expectThat(status).isEqualTo(MoneyTransferStatus.SUCCEED)
            expectThat(fromAccountId).isEqualTo(from.id)
            expectThat(toAccountId).isEqualTo(to.id)
        }
        with(accountManagement.findById(from.id)!!) {
            expectThat(balanceFor(currencyToTransfer).fromRawAmount()).isEqualTo(70.0)
        }
        with(accountManagement.findById(to.id)!!) {
            expectThat(balanceFor(currencyToTransfer).fromRawAmount()).isEqualTo(30.0)
        }
    }

    test("money transfer is thread safe") {
        // given
        val r = Random()
        val accountsCount = 10 // small number of accounts to have more chances for potential deadlock/concurrency issue
        val threadsCount = 16
        val transfersCount = 1_000_000
        val initialRawBalance = 1000.0.toRawAmount()
        val currency = Currency.USD

        val accounts = ArrayList<Account>(accountsCount)
        repeat(accountsCount) {
            val account = Account(idGenerator.nextId(), "Holder", mapOf(currency to initialRawBalance))
            accounts.add(account)
            accountManagement.updateAccount(account)
        }
        val executors = Executors.newFixedThreadPool(threadsCount)
        val tasks = ArrayList<Future<*>>(transfersCount)
        val nextRandomAccountId = {
            val nextAccountIndex = Math.abs(r.nextInt()) % accountsCount
            accounts[nextAccountIndex].id
        }
        val successfulTransfers = AtomicInteger()

        // when
        repeat(transfersCount) {
            val task = executors.submit {
                var transferred = false
                while (!transferred) {
                    val fromAccountId = nextRandomAccountId()
                    val toAccountId = nextRandomAccountId()
                    if (fromAccountId != toAccountId) {
                        val fromAccount = accountManagement.findById(fromAccountId)!!
                        val balance = fromAccount.balanceFor(currency)
                        val nextTransferSize = abs(r.nextInt(5)).toLong()
                        if (balance >= nextTransferSize) {
                            val transfer = sut.transfer(fromAccount.id, toAccountId, currency, nextTransferSize)
                            if (transfer.status == MoneyTransferStatus.SUCCEED) {
                                transferred = true
                                successfulTransfers.incrementAndGet()
                            }
                        }
                    }
                }
            }
            tasks.add(task)
        }
        tasks.forEach { it.get() } //wait for completion
        executors.shutdownNow()

        // then
        expectThat(successfulTransfers.get()).isEqualTo(transfersCount)

        val sumBalanceAfterTransfers = accounts.sumBy { each ->
            val account = accountManagement.findById(each.id)!! // to check actual persisted state
            account.balanceFor(currency).toInt()
        }.toLong()
        expectThat(sumBalanceAfterTransfers).isEqualTo(accountsCount * initialRawBalance)
    }
})
