# Money Transfer Application

## Requirements
 - Java 8+

## Building and running

```bash

./gradlew clean shadowJar

java -jar ./build/libs/money-transfer-1.0-SNAPSHOT-all.jar

```

## API

By default Application starts on port `8888`

Verb | Endpoint             | Description                     | Payload                                                                  |
---  | -------------------- | ------------------------------- | ------------------------------------------------------------------------ |
POST | /accounts            | Create account                  | {"accountHolder": "John Doe"}                                            |
GET  | /accounts/:id        | Query account                   |                                                                          |
POST | /accounts/:id/top-ups| Top up account                  | { "currency": "USD", "amount": 100}                                      |
POST | /transfers           | Transfer money between accounts | { "fromAccountId": 1, "toAccountId": 2, "currency": "USD", "amount": 50} |
GET  | /transfers/:id       | Query transfer                  |                                                                          |
